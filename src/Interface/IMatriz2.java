/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interface;

/**
 *
 * @author madar
 */
public interface IMatriz2 {
    
    /**
     * Método encuentra el número que más se repite
     * @return un entero de la colección de datos 
     */
    public int getMas_Se_Repite();
    
}
